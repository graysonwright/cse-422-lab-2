#include "HTTP_Request.h"
#include "HTTP_Response.h"
#include "URI.h"
#include <climits>
#include <cstdlib>
#include <cstring>
#include <cstdio>
#include <iostream>
#include <netdb.h>
#include <string>
#include <sstream>

using namespace std;

// prints a brief usage string describing how to use the application, in case
// the user passes in something that just doesn't work.
void help_message(const char* exe_name, ostream& out)
{
    out << "Usage: " << exe_name << " [options]" << endl;
    out << "The following options are available:" << endl;
    out << "    -s host URI " << endl;
    out << "    -p proxy URI" << endl;
    out << endl;
    out << "Example: " << exe_name 
        << "-s http://www.some_server.com/ -p 100.200.50.150:8080"  << endl;
}

/*********************************
 * Name:    parse_args
 * Purpose: parse the parameters
 * Receive: argv and argc
 * Return:  return by call by reference
 *          target_URI: the URI we are going to download
 *          proxy_addr: the address of the proxy
 *********************************/
int parse_args(int argc, char *argv[],
               char **uri_addr, char **proxy_addr)
{
    for(int i = 1; i < argc; i++)
    {
        if((!strncmp(argv[i], "-s", 2)) ||
           (!strncmp(argv[i], "-S", 2)))
        {
            *uri_addr = argv[++i];
        }
        else if((!strncmp(argv[i], "-p", 2)) ||
                (!strncmp(argv[i], "-P", 2)))
        {
            *proxy_addr = argv[++i];
        }
        else if((!strncmp(argv[i], "-h", 2)) ||
                (!strncmp(argv[i], "-H", 2)))
        {
            help_message(argv[0], cout);
            exit(1);
        }
        else
        {
            cerr << "Invalid parameter: argv[i]" << endl;
            help_message(argv[0], cout);
            exit(1);
        }
    }
}

/*
 * Purpose: Open a file pointer to store the data
 * Receive: The URI this program is downloading
 * Return:  The file pointer
 */
// Opens a local copy of the file referenced by the given request URI, for
// writing.  Ignores any directories in the URI path, instead opening the file
// in the current directory.  Makes up a filename if none is given.
//
// Returns a pointer to the open file, or a NULL pointer if the open fails.
FILE* Open_local_copy(const URI* uri)
{
    FILE* outfile = NULL;

    const string& full_path = uri->get_path();
    size_t filename_pos = full_path.rfind('/');

    if ((filename_pos != string::npos) &&
        ((filename_pos + 1) < full_path.length()))
    {
        outfile = fopen(full_path.substr(filename_pos + 1).c_str(),
            "wb");
    }
    else
    {
        outfile = fopen("index.html", "wb");
    }

    return outfile;
}





/*
 * Purpose: for a given data, extract the chunk_len
 * Receive: the data as string
 * Return:  the extracted chunk_len
 */
int get_chunk_size(string &data)
{
    int chunk_len;          // The value we want to obtain
    int chunk_len_str_end;  // The var to hold the end of chunk length string
    std::stringstream ss;   // For hex to in conversion

    chunk_len_str_end = data.find("\r\n"); // Find the first CLRF
    string chunk_len_str = data.substr(0, chunk_len_str_end);
    // take the chunk length string out

    // convert the chunk length string hex to int
    ss << std::hex << chunk_len_str;
    ss >> chunk_len;

    // reorganize the data
    // remove the chunk length string and the CLRF
    data = data.substr(chunk_len_str_end + 2, data.length() - chunk_len_str_end - 2);

    //cout << "chunk_len_str: " << chunk_len_str << endl;
    //cout << "chunk_len:     " << chunk_len << endl;  
    
    return chunk_len;
}
