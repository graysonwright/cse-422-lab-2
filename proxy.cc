#include "proxy.h"
#include "Proxy_Worker.h"

/*
 * Name:    sample_handler
 * Purpose: Handles a single web request
 * Receive: You decide
 * Return:  You decide
 */
void sample_handler(TCP_Socket &client_sock){
    // create a Proxy_Worker to handle this request
    Proxy_Worker worker(&client_sock);
    worker.handle_request();
}

/*
 * Purpose: Contains the main server loop that handles requests by 
 *          spawing child processes
 * Receive: argc is the number of command line params, argv are the 
 *          parameters
 * Return:  0 on clean exit, and 1 on error
*/
int main(int argc, char *argv[]) {
    // Parse arguments (make sure there are no options passed in)
    parse_args(argc, argv);

    // WE NEED A SOCKET FOR ACCEPTING INCOMING CONNECTION
    TCP_Socket listen_sock;
    // THE SOCKET TO HANDLE EACH CLIENT
    TCP_Socket client_sock;

    pid_t pid;

    // SET UP THE LISTENING SOCK
    listen_sock.Bind(0);
    listen_sock.Listen();

    unsigned short int port = listen_sock.get_port();
    cout << "Proxy running at " << port << "..." << endl;

    int i = 0;
    //start the infinite server loop
    while(i < 100) { // To avoid overruning the host
        // accept incoming connections
        listen_sock.Accept(client_sock);
        i++; // i is the number of current child processes
             // If you are certain that your program does not 
             // overrun the machine, remove statements related
             // to i.

        // using client_sock

        // create new process
        // using fork
        pid = fork();

        // if pid < 0, the creation of process is failed.
        if(pid < 0)
        {
            cerr << "Unable to fork new child process." << endl;
            exit(1);
        }
        // if pid == 0, this is the child process
        else if(pid == 0)
        {
            cout << "[SYS] Child process created" << endl;
            // Child process invoke a ConnectionHandler function
            // to handle this connection
            sample_handler(client_sock);

            // close the client_sock if necessary

            cout << "[SYS] Child process teminated" << endl;
            exit(0);
        }
        // if pid > 0, this is the parent process
        // Parent process continues the loop to accept new incoming 
        // connections.

        // The child process has done handleing this connection
        // Terminate the child process
    }

    // The parent process

    // close the listening sock
    listen_sock.Close();
    // output a message saying that the server process is terminated
    cout << "[SYS] Terminating proxy server" << endl;

    return 0;
}

