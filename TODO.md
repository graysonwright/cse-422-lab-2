## TODO

## In Progress

## Done

- forwards GET requests from a client to the server
- returns the responses from the server back to the client
- port number assigned by operating system
- non-persistent connections
- handle multiple requests by forking instances for each request
- handle default encoding and chunked transfer encoding

- respond with error messages to bad requests
  - if path doesn't exist, end server will respond with 404
  - if server doesn't exist, need to generate 404

- perform simple filtering
  - return `403 Forbidden` when host contains 'facebook'
- handle default transfer encoding and chunked transfer encoding
  - default: proxy displays content length
  - chunked: proxy displays length of all chunks

- modify server field in the response header with string (such as NetID)
  - HTTP_Response::set_header_field

- README & documentation
