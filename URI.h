// URI - Represents most aspects of a uniform resource identifier (URI).  Can
// be used to parse existing URI strings into their component parts, and to form
// new URI strings piece-by-piece from those components.
//
// Expects URIs to be formatted in the following manner (note that most fields
// are optional, depending on which other fields are also present):
//
// protocol://host:port/path?query#fragment

//
// Modified/Adopted from CSE422 FS09 TA
#ifndef _URI_H_
#define _URI_H_

#include <iostream>
#include <string>


class URI
{
public:
    URI();
    ~URI();

    // Creates a new URI based on the contents of the given string.
    // A few things to note about how the parsing is done:
    //  + The protocol *must* be specified.  "http://example.org" will
    //    parse. http:// will be the default protocl if not given
    //  + If no port number is given in the URI, the returned URI object
    //    will have the port clearly indicates as being undefined.
    //  + If no path is given in the URI, it will be set to a forward slash
    //    i("/"), to avoid having a blank string there.
    //
    //
    // Name:    parse
    // Purpose: As described above
    // Recevie: uri_string - The URI string to parse.
    // Return:  A URI object with its components taken from the given
    //          string. If the given string is not formatted like a proper 
    //          URI and cannot be parsed, a NULL pointer will be returned 
    //          instead.
    static URI* parse(const std::string& uri_string);


    // Purpose: Looks up the protocol given in the URI.
    // Recevie: None
    // Return:  The URI's protocol.
    const std::string& get_protocol() const;

    // Purpose: Looks up the target host of the URI.
    // Receive: None
    // Return:  The URI's host.
    const std::string& get_host() const;

    // Purpose: Checks if the URI refers to a specific port, or none at all.
    // Receive: None
    // Return:  true if the URI has a defined port, false if not.
    bool is_port_defined() const;

    // Purpose: Looks up the port number to which the URI refers.
    // Receive: None
    // Return:  The URI's port.  If is_port_defined() returns false, this
    //          value is meaningless.
    unsigned get_port() const;

    // Purpose: Looks up the path of the resource to which the URI refers.
    // Receive: None
    // Return:  The URI's path.
    const std::string& get_path() const;

    // Purpose: Looks up the query part of the URI (which may be used to identify
    //          a resource in a non-hierarchical manner, unlike the path).
    // Receive: None
    // Return:  The URI's query.
    const std::string& get_query() const;

    // Purpose: Looks up the fragment of the primary resource to which the URI
    //          specifically refers (e.g. an anchor in a web page).
    // Receive: None
    // Return:  The URI's fragment.
    const std::string& get_fragment() const;


    // Purpose: Has the URI printed to the given output stream, in standard format.
    // Receive: out - The output stream to which to print the URI.
    // Return:  None
    void Print(std::ostream& out) const;

    // Purpose: Has the URI printed into the given string, in standard format.
    // Receive: target - Will be set to a string representation of this URI.
    // Return:  None
    void Print(std::string& target) const;


    // Purpose: Sets the URI protocol to the given string.
    // Receive: protocol - The protocol to set.
    // Return:  None
    void set_protocol(const std::string& protocol);

    // Purpose: Sets the URI's host to the given string.
    // Receive: host - The host to set.
    // Return:  None
    void set_host(const std::string& host);

    // Purpose: Throws out the port of the URI, making it undefined.
    // Receive: None
    // Return:  None
    void Clear_port();

    // Purpose: Sets a specific port number for the URI.
    // Receive: port - The port to set.
    // Return:  None
    void set_port(unsigned port);

    // Sets the path of the resource to which the URI refers.
    // Receive: None
    //
    // path - The path to set.
    void set_path(const std::string& path);

    // Sets the query string for the URI.
    // Receive: None
    //
    // query - The query to set.  Can be used to effectively delete the
    //   URI's query by passing in a blank string.
    void set_query(const std::string& query);

    // Sets the fragment for the URI.
    // Receive: None
    //
    // fragment - The fragment to set.  Will effectively delete the URI's
    //   fragment if you pass in a blank string.
    void set_fragment(const std::string& fragment);

public:
    friend std::ostream& operator<<(std::ostream& out, const URI& uri)
    {
        std::string buffer;
        uri.Print(buffer);
        out << buffer.substr(0, buffer.length()) << std::endl;
        return out;
    }

    friend std::ostream& operator<<(std::ostream& out, const URI* uri)
    {
        std::string buffer;
        uri->Print(buffer);
        out << buffer.substr(0, buffer.length()) << std::endl;
        return out;
    }

private:
    size_t read_protocol(const std::string& uri_string, size_t offset = 0);
    size_t read_host_port(const std::string& uri_string, size_t offset);
    size_t read_path_details(const std::string& uri_string, size_t offset);

    std::string protocol;
    std::string host;
    unsigned port;
    std::string path;
    std::string query;
    std::string fragment;
};

#endif //ndef _URI_H_
