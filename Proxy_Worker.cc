#include "Proxy_Worker.h"
#include <sstream>
#include <algorithm>
#include "client.h"
using namespace std;

/*
 * Purpose: constructor
 * receive: cs is the socket that is already connected to the requesting 
 *          client
 * Return:  none
 */
Proxy_Worker::Proxy_Worker(TCP_Socket *cs) {
    client_sock = cs;
    port = 80; // For a full blown proxy, the server information should be 
               // obtained from each request. However, we simply assume it 
               // to be 80 for our labs.
    server_uri = NULL;
               // Must be obtain from each request.
}

/*
 * Purpose: destructor
 * receive: none
 * Return:  none
 */
Proxy_Worker::~Proxy_Worker() {
    // destructor
    // Please do try to destruct the variables.
}

/*
 * Purpose: handles a request by requesting it from the server_uri 
 * Receive: none
 * Return:  none
 * 
 *
 * This function is implementation of Handout 4.1
*/
void Proxy_Worker::handle_request() {
    // Get the request, check if the request is valid by parsing it.
    // (parsing is done in HTTP_Request::receive) 
    // From the parsed request, obtain the server address (in code, 
    // server_uri)
    cout << "Getting request from client..." << endl;
    if(!get_request_from_client()) 
    {
        cerr << "No request from client!" << endl;
        return;
    }

    // Just outputting the request.
    cout << "Received request:" << endl;
    cout << "==========================================================" << endl;
    cout << client_request;
    cout << "==========================================================" << endl << endl;

    // Checking the request
    cout << "Checking request..." << endl;
    // 2. filter out any host with the keyword "facebook"
    string host = server_uri->get_host();
    transform(host.begin(), host.end(), host.begin(), ::tolower);
    size_t facebook_start = host.find("facebook");
    if (facebook_start != string::npos) {
        //    Note that we are filtering out "host with keyworkd facebook"
        //    Paths with facebook are not filtered
        cout << "Found host with facebook" << endl;
        //    Respond a 403 forbidden for host with facebook.
        return_response_to_client(403);
        return;
    }
    // 1. make sure we're pointing to a server
    TCP_Socket server_sock;
    try{
        // Connect to the target server.
        cout << "Trying to connect to " << server_uri << endl;
        server_sock.Connect(*server_uri);
        cout << "Connection Successful!" << endl;
        server_sock.Close();
    }
    // Give up if sock is not created correctly.
    catch(string msg)
    {
        cout << msg << endl;
        cout << "Unable to connect to server: " 
            << server_uri->get_host() << endl;
        //    Respond a 404 Not Found if the server is invalid
        return_response_to_client(404);
        return;
    }

    cout << "Request valid and allowed." << endl;

    // Forward the request to the server
    // Receive the response header and modify the server header field
    // Receive the response body. Handle the default and chunked transfer
    // encoding.
    if(!forward_request_to_server()) {
        cout << "Problem forwarding request to server and getting response." << endl;
        return;
    }

    return;
}

/*
 * Purpose: receives the request from a client and parse it.
 * Receive: None
 * Return:  a boolean indicating if getting the request was succesful or not
 */
bool Proxy_Worker::get_request_from_client() {
    // Get the request from the client (HTTP_Request::receive)
    // Obtain the server_uri from the request (HTTP_Request::get_host)
    client_request = HTTP_Request::receive(*client_sock);
    server_uri = URI::parse(client_request->get_host());
    return(client_request != NULL);
}

/*
 * Purpose:
 *          Forward the request to the server
 *          Receive the response header and modify the server header field.
 *          Receive the response body. Handle the default and chunked transfer.
 * Receive: None
 * Return:  a boolean indicating if forwarding the request was succesful or not
 *
 *
 * Forward the request to the server
 * Receive the response header and modify the server header field
 * Receive the response body. Handle the default and chunked transfer
 * encoding.
 */
bool Proxy_Worker::forward_request_to_server() {
    // pass the client request to the server

    // First, connect to the server
    // (already connected in handle_request)

    // send the request to the server
    // Send a GET request for the specified file.
    // No matter connecting to the server or the proxy, the request is 
    // alwasy destined to the server.
    HTTP_Request *request = HTTP_Request::create_GET_request(server_uri->get_path());
    request->set_host(server_uri->get_host());
    request->set_header_field("Connection", "close");
    request->set_uri(client_request->get_uri());

    server_sock.Connect(*server_uri);
    try
    {
        request->send(server_sock);
    }
    catch(string msg)
    {
        cerr << msg << endl;
    }

    // output the request
    cout << "Request sent..." << endl;
    cout << "==========================================================" 
         << endl;
    string print_buffer;
    request->print(print_buffer);
    cout << print_buffer.substr(0, print_buffer.length() - 4) << endl;
    cout << "==========================================================" 
         << endl;

    delete request; // We do not need it anymore
    /***END OF SENDING REQUEST***/


    // receive the response header from the server
    string response_header, response_data;
    try {
        HTTP_Response::receive_header(server_sock, response_header, response_data);
    }
    catch(string msg) {
        cerr << msg << endl;
        return false;
    }
    cout << "Received Header" << endl;
    server_response = HTTP_Response::parse(response_header.c_str(), 
                                    response_header.length());
    cout << "Parsed response header" << endl;

    // The response is illegal.
    if(server_response == NULL)
    {
        cerr << "Unable to parse the response header." << endl;
        // clean up if there's something wrong
        delete server_response;
        delete server_uri;
        return false;
    }

    // output the response header
    cout << endl << "Response header received" << endl;
    cout << "=========================================================="
         << endl;
    cout << server_response;
    cout << "==========================================================" 
         << endl;

    /***END OF RECEIVING RESPONSE HEADER FROM THE SERVER***/

    // check the response header, is it default encoding or chunked
    // receive response body
    int bytes_written = 0, bytes_left, total_data;
    ostringstream out;
    if(server_response->is_chunked() == false)
    {
        cout << "Default encoding transer" << endl;
        cout << "Content-length: " << server_response->get_content_len() << endl;
        bytes_left = server_response->get_content_len();
        do
        {
            // If we got a piece of the file in our buffer for the headers,
            // have that piece written out to the file, so we don't lose it.
            out << response_data;
            bytes_written += response_data.length();
            bytes_left -= response_data.length();
            //cout << "bytes written:" <<  bytes_written << endl;
            //cout << "data gotten:" <<  response_data.length() << endl;

            response_data.clear();
            try
            {
                // Keeps receiving until it gets the amount it expects.
                server_response->receive_data(server_sock, response_data, 
                                       bytes_left);
            }
            catch(string msg)
            {
                // something bad happend
                cout << msg << endl;
                // clean up
                delete server_response;
                delete server_uri;
                server_sock.Close();
                return false;
            }
        } while (bytes_left > 0);
    }
    else // chunked encoding
    {
        // The client program does not handle it for now.
        cout << "Chunked encoding transfer" << endl;

        int chunk_len = get_chunk_size(response_data);
        cout << "chunk length: " << chunk_len << endl;
        total_data = chunk_len;
        while(1)
        {
            // If current data holding is less than the chunk_len, this 
            // piece of data contains part of this chunk. Receive more
            // until we have a complete chunk to store!
            if(response_data.length() <= chunk_len)
            {
                try
                {
                    // receive more until we have the whole chunk.
                    server_response->receive_data(server_sock, response_data, 
                                (chunk_len - response_data.length()));
                    server_response->receive_line(server_sock, response_data);
                    // get the blank line between chunks
                    server_response->receive_line(server_sock, response_data);
                    // get the next chunk size
                }
                catch(string msg)
                {
                    // something bad happend
                    cout << msg << endl;
                    // clean up
                    delete server_response;
                    delete server_uri;
                    server_sock.Close();
                    return false;
                }
            }
            // If current data holding is longer than the chunk size, this
            // piece of data contains more than one chunk. Store the chunk.
            else//response_data.length() >= chunk_len
            {
                out << response_data;
                bytes_written += chunk_len;

                // reorganize the data, remove the chunk from it
                // the + 2 here is to consume the extra CLRF
                response_data = response_data.substr(chunk_len + 2, 
                                response_data.length() - chunk_len - 2);
                //get next chunk size
                chunk_len = get_chunk_size(response_data);
                total_data += chunk_len;
                cout << "chunk length: " << chunk_len << endl;
                if(chunk_len == 0)
                {
                    break;
                }
            }
        }
    }

    string data = out.str();
    bool result = return_response_to_client(server_response->get_status_code(), data);

    // close the connection
    server_sock.Close();

    return result;
}

/*
 * Purpose: Return a response to a client
 * Receive: None
 * Return:  a boolean indicating if returning the request was succesful 
 *          or not (always true for now)
*/
bool Proxy_Worker::return_response_to_client(int code, string &data) {
    HTTP_Response *client_response = HTTP_Response::create_standard_response(data.size(), code);
    client_response->set_content(data);

    HTTP_Header_Field server;
    server.name = "Server";
    server.value = "CSE 422 Proxy (wrightg7)";
    client_response->set_header_field(server);

    cout << endl << "Sending response:" << endl;
    cout << "=========================================================="
         << endl;
    cout << client_response;
    cout << "==========================================================" 
         << endl;
    client_response->send(*client_sock);
    return true;
}

/*
 * Purpose: Return a response to a client with no content
 * Receive: None
 * Return:  a boolean indicating if returning the request was succesful 
 *          or not (always true for now)
 *
 * This is for returning 403, 404 ... those error messages.
*/
bool Proxy_Worker::return_response_to_client(int status_code) {
    map<int,string> codeNames;
    codeNames[403] = "Forbidden";
    codeNames[404] = "Not Found";
    cout << "Sending response with code " << status_code << ": " << codeNames[status_code] << "." << endl;
    HTTP_Response client_response(status_code, codeNames[status_code]);

    HTTP_Header_Field server;
    server.name = "Server";
    server.value = "CSE 422 Proxy (wrightg7)";
    client_response.set_header_field(server);

    client_response.send(*client_sock);
}

